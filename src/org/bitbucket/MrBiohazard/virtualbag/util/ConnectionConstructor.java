package org.bitbucket.MrBiohazard.virtualbag.util;

import org.bitbucket.MrBiohazard.virtualbag.xml.DatabaseConnection;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionConstructor {
    public static Connection createConnection(DatabaseConnection connectionInfo) throws MalformedURLException, ClassNotFoundException, SQLException {
        URLClassLoader driverLoader = new URLClassLoader(new URL[]{new URL(connectionInfo.driverInfo.driverFile)});
        System.out.println("Attempting to load JDBC Driver: " + connectionInfo.driverInfo.driverFile);
        Class.forName(connectionInfo.driverInfo.driverClass, true, driverLoader);
        return DriverManager.getConnection(connectionInfo.jdbc.url, connectionInfo.userdata.username, connectionInfo.userdata.password);
    }
}

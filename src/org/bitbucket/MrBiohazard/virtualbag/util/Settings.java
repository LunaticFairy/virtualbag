package org.bitbucket.MrBiohazard.virtualbag.util;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import org.bitbucket.MrBiohazard.virtualbag.xml.DatabaseConnection;

import java.io.FileNotFoundException;
import java.io.FileReader;

public class Settings {
    private static XStream xstream = new XStream(new DomDriver());

    public static DatabaseConnection getConnectionData() throws FileNotFoundException {
        xstream.alias("connection", DatabaseConnection.class);
        xstream.aliasField("driver", DatabaseConnection.class, "driverInfo");
        return (DatabaseConnection) xstream.fromXML(new FileReader("sqlsettings.xml"));
    }
}

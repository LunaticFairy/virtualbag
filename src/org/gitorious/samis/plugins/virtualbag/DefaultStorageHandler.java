package org.gitorious.samis.plugins.virtualbag;

import java.io.*;
import java.util.Collections;
import java.util.List;

public class DefaultStorageHandler implements StorageHandler {
    private ObjectInputStream bagInput;
    private File outputfile;
    private List<BagItem> bagItems;

    public DefaultStorageHandler() {
        this("bag.dat");
    }

    public DefaultStorageHandler(String outputfile) {
        this.outputfile = new File(outputfile);
    }

    @Override
    public boolean thereIsSavedData() {
        return outputfile.exists();
    }

    @Override
    public List<BagItem> restoreBag() {
        bagItems = Collections.emptyList();
        try {
            bagInput = new ObjectInputStream(new FileInputStream(outputfile));
            bagItems = loadBag();
        } catch (FileNotFoundException e) {
            reportIOError(e);
        } catch (ClassNotFoundException e) {
            reportClassNotFound(e);
        } catch (IOException e) {
            reportIOError(e);
        } finally {
            closeStream(bagInput);
        }
        return bagItems;
    }

    private List<BagItem> loadBag() throws IOException, ClassNotFoundException {
        return (List<BagItem>) bagInput.readObject();
    }

    private void reportIOError(IOException e) {
        System.err.println("An IO Error Occured");
        e.printStackTrace();
    }

    private void reportClassNotFound(ClassNotFoundException e) {
        System.err.println("A class was not found");
        e.printStackTrace();
    }

    private void closeStream(Closeable stream) {
        try {
            stream.close();
        } catch (IOException e) {
            reportIOError(e);
        }
    }

    @Override
    public void updateBag(List<BagItem> items) {
        if (items.equals(bagItems)) {
            return;
        } else {
            writeBag(items);
        }
    }

    private void writeBag(List<BagItem> items) {
        try {
            writeItems(items);
        } catch (FileNotFoundException e) {
            reportIOError(e);
        } catch (IOException e) {
            reportIOError(e);
        }
    }

    private void writeItems(List<BagItem> items) throws IOException {
        ObjectOutputStream bagOutput = new ObjectOutputStream(new FileOutputStream(outputfile));
        bagOutput.writeObject(items);
        closeStream(bagOutput);

    }

}

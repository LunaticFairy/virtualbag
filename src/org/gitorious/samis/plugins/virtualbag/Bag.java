package org.gitorious.samis.plugins.virtualbag;

import java.util.*;

public class Bag {
    private List<BagItem> items;
    private StorageHandler storagehandler;
    private static final int INDEXOFFSET = 1;
    private static final int STARTINGCAPACITY = 10;

    public Bag() {
        this(new DefaultStorageHandler());
    }

    public Bag(StorageHandler storageHandler) {
        super();
        this.storagehandler = storageHandler;
        this.initializeBag();
    }

    private void initializeBag() {
        if (this.storagehandler.thereIsSavedData()) {
            this.items = Collections.emptyList();
            this.items = this.storagehandler.restoreBag();
        } else {
            this.items = new ArrayList<BagItem>(Bag.STARTINGCAPACITY);
        }

    }

    public void addItem(BagItem item) {
        this.items.add(item);
        this.updateBag();
    }

    protected void updateBag() {
        this.storagehandler.updateBag(items);

    }

    public BagItem retrieveItem() {
        return getRandomItem();
    }

    public BagItem retrieveItem(BagItem item) {
        if (!this.items.contains(item)) {
            throw new IllegalArgumentException(
                    "retrieveItem() was passed an item which is not in the bag");
        }
        int bagitemindex = this.items.indexOf(item);
        BagItem bagitem = this.items.get(bagitemindex);
        this.items.remove(bagitem);
        updateBag();
        return bagitem;
    }

    public void removeItem(BagItem item) {
        retrieveItem(item);
        updateBag();
    }

    public boolean isEmpty() {
        return this.items.isEmpty();
    }

    public void removeAllItems() {
        this.items.clear();
    }

    protected BagItem getRandomItem() {
        int numberOfItems = this.items.size() - Bag.INDEXOFFSET;
        Random indexGenerator = new Random();
        int indexOfItem = indexGenerator.nextInt(numberOfItems);
        return this.items.get(indexOfItem);
    }

    protected StorageHandler getStoragehandler() {
        return this.storagehandler;
    }

    protected void setStoragehandler(StorageHandler storageHandler) {
        this.storagehandler = storageHandler;
    }

    public String inventory() {
        StringBuilder builder = new StringBuilder(Bag.STARTINGCAPACITY);
        builder.append("The Following items are in the bag:");
        builder.append(this.items);
        return builder.toString();
    }

    @Override
    public String toString() {
        return "Bag{" +
                "items=" + this.items +
                ", storagehandler=" + this.storagehandler +
                '}';
    }
}

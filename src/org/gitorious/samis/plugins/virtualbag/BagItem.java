package org.gitorious.samis.plugins.virtualbag;

import java.io.Serializable;

public class BagItem implements Serializable {

    public String item;
    public String owner;

    public BagItem(String item, String owner) {
        super();
        this.item = item;
        this.owner = owner;
    }

    @Override
    public String toString() {
        return owner + "" + "'s" + "" + item;
    }
}

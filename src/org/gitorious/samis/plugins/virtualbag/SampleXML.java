package org.gitorious.samis.plugins.virtualbag;

public class SampleXML {
	
    public static void main(String[] args) {
        XMLStorageHandler handler = new XMLStorageHandler();
        Bag bag = new Bag(handler);
        String owner = "CompanionCube";
        for (int i = 0; i < 20; i++) {
            String item_string = "Item" + String.valueOf(i);
            BagItem item = new BagItem(owner, item_string);
            bag.addItem(item);
        }
    }
}

package org.gitorious.samis.plugins.virtualbag;

import java.util.List;

public interface StorageHandler {

    List<BagItem> restoreBag();

    void updateBag(List<BagItem> items);

    boolean thereIsSavedData();

}

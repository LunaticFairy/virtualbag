package org.gitorious.samis.plugins.virtualbag;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class XMLStorageHandler implements StorageHandler {
    File xml;
    XStream xStream;
    BufferedWriter xmlWriter;

    public XMLStorageHandler() {
        this("bag.xml");
    }

    public XMLStorageHandler(String file) {
        xStream = new XStream(new DomDriver());
        xStream.alias("bagitem", BagItem.class);
        xml = new File(file);
    }

    @Override
    public List<BagItem> restoreBag() {
        if (thereIsSavedData()) {
            return (List<BagItem>) xStream.fromXML(xml);
        } else {
            return Collections.emptyList();
        }
    }

    @Override
    public void updateBag(List<BagItem> items) {
    	//TODO Finish this.
        try {
            xmlWriter = new BufferedWriter(new FileWriter(xml));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String xmlData = xStream.toXML(items);
        xml.delete();
        try {
            xmlWriter.write(xmlData);
            xmlWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean thereIsSavedData() {
        return xml.exists();
    }
}
